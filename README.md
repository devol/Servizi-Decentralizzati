I like to use services i can donate to, which are thrustworhty and don't have
advertisements. I think, our society would greatly profit from this financial
model. Therefore, comercial providers are rare here. Therefore, please
donate to those projects, especially if you use them much.

Since ages, i have maintained this list of tech collectives. Especially,
because i always forget the names of some services in here.

I decided to make this list public and donate it to the [public
domain](./LICENSE.md). Patches welcome.

Also, i want to encourage you to go to your favorite project using for instance
the mailinglist feature of google groups and suggest them one of these
alternatives.

**Please donate to the services you use! Even 1 euro/month is helpful.**

#### VPN
* [aktivix.org](https://aktivix.org/), activistic

#### demoticker
* [systemli.org](https://www.systemli.org/service/ticker.html), activistic
* [systemausfall.org](https://systemausfall.org/dienste/weitere-dienste), activistic

#### doodle
* [disroot.org](https://disroot.org/en/services/polls), activistic
* [nomagic.uk](https://nomagic.uk/services/#EventScheduler)
* [whatapoll.de](https://whatapoll.de/)
* [nuudel.digitalcourage.de](https://nuudel.digitalcourage.de/)

#### metadata cleaner
* [metadata.systemli.org](https://metadata.systemli.org/), activistic

#### doodle, end-to-end encrypted
* [systemli.org](https://www.systemli.org/service/croodle.html), activistic

#### shift plan
* [immerda.ch](https://schichtplan.immerda.ch/), activistic

#### projectverwaltung
* [systemausfall.org](https://systemausfall.org/dienste/projektverwaltung), software used: redmine, activistic
* [tree.tchncs.de](https://tree.tchncs.de/discover), software used: taiga
* [board.disroot.org](https://disroot.org/en/services/project-board), software used: taiga, activistic
* [nomagic.uk](https://nomagic.uk/services/#Kanban), kanban board
* [kanban.adminforge.de](https://kanban.adminforge.de/)

#### permanent filehosting
* [wiuwiu.de](https://wiuwiu.de), nextcloud
* [cloud.tchncs.de](https://cloud.tchncs.de/login), nextcloud
* [disroot.org](https://disroot.org/en/services/nextcloud), activistic, nextcloud
* [nomagik.uk](https://nomagic.uk/services/#FileHosting), seafile

#### Link shortener
* [s.devol.it](https://s.devol.it)
* [frama.link](https://frama.link/)
* [nxnt.link](https://nxnt.link/)
* [link.infini.fr](https://link.infini.fr/)
* [donotlink.it](https://donotlink.it/)
* [shorter.ggc-project.de](https://shorter.ggc-project.de/)
* [l.anjara.eu](https://l.anjara.eu/)
* [fckaf.de](https://fckaf.de/), made by the political party `Die Partei`
* [nomagic.uk](https://nomagic.uk/services/####URLShortener), login required, UK
* [t1p.de](https://t1p.de/), company behind

#### temporary image upload
* [nomagic.uk](https://nomagic.uk/services/#TemporaryImageHosting)

#### temporary file uploading
* [jirafeau](https://jirafeau.devol.it)
* [share.riseup.net](https://share.riseup.net/)
* [upload.disroot.org](https://upload.disroot.org/), max 1GB, activistic 
* [nomagic.uk](https://nomagic.uk/services/#TemporaryFileHosting)
* [nowtransfer.de](https://nowtransfer.de/)
* [files.anjara.eu](https://files.anjara.eu/)

#### survey
* [nomagic.uk](https://nomagic.uk/services/#OnlineSurvey), UK

#### pastebin
* [privatebin.devol.it](https://privatebin.devol.it/)
* [ix.io](http://ix.io)
* [sprunge.us](http://sprunge.us/)
* [bin.nixnet.xyz](https://bin.nixnet.services/)
* [hastebin.wiuwiu.de](https://hastebin.wiuwiu.de/)
* [haste.tchncs.de](https://haste.tchncs.de/)
* [paste.ggc-project.de](https://paste.ggc-project.de/)

#### etherpad
* [etherpad.devol.it](https://etherpad.devol.it)
* [pad.libreops.cc](https://pad.libreops.cc/) 
* [pad.systemli.org](https://pad.systemli.org), activistic
* [pad.adminforge.de](https://pad.adminforge.de/)
* [pad.c3w.at](https://pads.c3w.at/)

#### calc
* [calc.adminforge.de](https://calc.adminforge.de/)
* [calc.systemli.org](https://calc.systemli.org/), activistic

#### proxied websites
* [tor-docs.nixnet.xyz](https://tor-docs.nixnet.xyz/docs/documentation.html.en)
* [tor-bridges.nixnet.xyz](https://tor-bridges.nixnet.xyz/)
* [ssd.eff.nixnet.xyz](https://ssd-eff.nixnet.xyz/)

#### peertube
* [peertube.uno](https://peertube.uno)
* [conf.tube](https://conf.tube)
* [peertube.debian.social](https://peertube.debian.social)
* [pony.tube](https://pony.tube)
* [devtube.dev-wiki.de](https://devtube.dev-wiki.de/)
* [peertube.social](https://peertube.social)
* [tube.backbord.net](https://tube.backbord.net)
* [tube.tchncs.de](https://tube.tchncs.de)
* [tube.rebellion.global](https://tube.rebellion.global), instance of Extinction Rebellion
* [peertube.nomagic.uk](https://peertube.nomagic.uk/)

#### jabber/xmpp
* [honigdachse.de](https://honigdachse.de)
* [magicbroccoli.de](https://magicbroccoli.de)
* [disroot.org](https://disroot.org/en/services/xmpp), activistic
* [dismail.de](https://dismail.de)
* [wiuwiu.de](https://wiuwiu.de/), access via Tor hidden service possible
* [nixnet.xyz](https://nixnet.services/jabber-xmpp/)
* [immerda.ch](https://immerda.ch/), activistic
* [systemausfall.org](https://systemausfall.org/dienste/xmpp-chat), activistic, access via Tor hidden service possible
* [nomagic.uk](https://nomagic.uk/services/#InstantMessaging), UK
* [account.conversations.im](https://account.conversations.im/)
* [pimux.de](https://www.pimux.de/)
* [trashserver.net](https://trashserver.net/)

#### matrix
* [ggc-project.de](https://www.ggc-project.de/sites/matrix.html)
* [tchncs.de](https://tchncs.de/matrix)
* [riot.linux.pizza](https://riot.linux.pizza/)

#### rocket.chat
* [chat.darknebu.la](https://chat.darknebu.la/)

#### mumble
* [snopyta.org](https://snopyta.org/)
* [libreops.cc](https://libreops.cc/mumble.html)
* [tchncs.de](https://tchncs.de/mumble)
* [it-kollektiv.com](https://todon.nl/@IT_Kollektiv/103856037781689376)

#### jitsi-meet
* [offical list of jitsi instances](https://github.com/jitsi/jitsi-meet/wiki/Jitsi-Meet-Instances)
* [talk.snopyta.org](https://talk.snopyta.org/)
* [meet.darknebu.la](https://meet.darknebu.la/)
* [meet.immerda.ch](https://meet.immerda.ch/), activistic
* [kuketz-meet.de](https://kuketz-meet.de/)
* [disroot.org](https://disroot.org/en/services/calls), activistic
* [it-kollektiv.com](https://todon.nl/@IT_Kollektiv/103856037781689376)
* [meet.linus-neumann.de](https://meet.linus-neumann.de/)
* [meet.adminforge.de](https://meet.adminforge.de)

#### mailinglist
* [disroot.org](https://disroot.org/en/services/forum), activistic, "Mailing list and forum in one"
* [aktivix.org](https://aktivix.org/), activistic
* [datenkollektiv.net](https://www.datenkollektiv.net/angebote/mailinglisten/), activistic
* [diskuti.gr](https://diskuti.gr/)
* [immerda.ch](https://www.immerda.ch), activistic
* [systemli.org](https://systemli.org), activistic
* [datenkollektiv.net](https://www.datenkollektiv.net/)
* [posteo.de](https://posteo.de), by request, comercial
* [resist.ca](https://lists.resist.ca/), activistic

#### mailinglist, end-to-end encrypted:
* [systemli.org](https://www.systemli.org/service/schleuder.html), activistic
* [systemausfall.org](https://systemausfall.org/dienste/mailinglisten), activistic
* [nadir.org](https://nadir.org/), activistic

#### personal mail
* [protonmail.com](https://protonmail.com)
* [immerda.ch](https://www.immerda.ch), activistic
* [aktivix.org](https://aktivix.org/), activistic
* [shleter.is](https://www.shelter.is/), activistic
* [datenkollektiv.net](https://www.datenkollektiv.net/)
* [dismail.de](https://dismail.de)
* [riseup.net](https://riseup.net/), actiivstic
* [resist.ca](https://resist.ca/), activistic
* [nixnet.email](https://nixnet.email/)

#### temporary mail
* [dismal.de](https://dismail.de)
* [tempmail.linux.pizza](https://tempmail.linux.pizza)
* [anonbox.net](https://anonbox.net/)
* [mailhole.de](https://mailhole.de)

#### "What is my IP address"websites
* [ip.bugsi.de](http://ip.bugsi.de/)
* [ip.darknebu.la](https://ip.darknebu.la/)
* [ipwho.de](https://ipwho.de/)

#### speed test
* [librespeed.org](https://librespeed.org)
* [bandspeed.de](https://bandspeed.de/)


#### invidious
see [this](https://instances.invidio.us) link.

#### create picture from text
* [privacy-captcha.net](https://privacy-captcha.net/) by digitalcourage.de,
  [german blog article][2]

#### webhosting, wordpress, blogs
* [noblogo.org](https://noblogo.org)
* [immerda.ch](https://www.immerda.ch), activistic
* [datenkollektiv.net](https://www.datenkollektiv.net/preise/)
* [shelter.is](https://www.shelter.is/), activistic
* [autistici.org](https://autistici.org), activistic
* [nostate.net](https://nostate.net/),activistic

#### gitlab, gitea, etc.
* [gitea.it](https://gitea.it)
* [git.snopyta.org](https://git.snopyta.org/)
* [git.nixnet.xyz](https://git.nixnet.xyz/)
* [git.ggc-project.de](https://git.ggc-project.de/)
* [git.tchncs.de](https://git.tchncs.de/)

#### dyn dns
* [noip.at](https://noip.at/)

#### dns server
* [libredns.gr](https://libredns.gr/)
* [digitalcourage.de](https://digitalcourage.de/support/zensurfreier-dns-server)
* [nixnet.services](https://nixnet.services/)
* [dismail.de](https://dismail.de/info.html#dns), ad & malware blocker, DNS over TLS
* [dnsforge.de](https://dnsforge.de/)

#### search engines
* [searx.devol.it](https://searx.devol.it)
* [searchforplanet.org](https://searchforplanet.org)
* [searx.nixnet.xyz](https://searx.nixnet.xyz/)
* [searx.anjara.eu](https://searx.anjara.eu/searx/)
* [suche.honigdachse.de](https://suche.honigdachse.de/)
* [disroot.org](https://disroot.org/en/services/search)
* [s.ggc-project.de](https://s.ggc-project.de/)
* [searx.linux.pizza](https://searx.linux.pizza/)

#### other lists, similar to this one
* [libreho.st](https://libreho.st/)
* [s036.net](https://so36.net/), activistic
* [riseup.net](https://riseup.net/de/security/resources/radical-servers), activistic


# what does "activistic" mean?

It does mean that they have a high reputation in the civil disobedience
movement. Mostly, i got these services from systemli.org, who maintains a
[list][1] of friends and fellows.


[1]: https://www.systemli.org/en/friends.html
[2]: https://digitalcourage.de/blog/2014/privacy-captcha-schuetze-deine-privaten-daten-vor-automatisierter-verarbeitung
